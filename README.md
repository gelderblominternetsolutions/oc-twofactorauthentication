#Two Factor Authentication for admin panel
##Description
A secure and easy to install Two Factor Authentication extension for the admin panel! Further strengthen Open Cart security with a 6 digit code, renewed every 30 seconds, for each log-in. This extension gives each administrator user the change to protect their account by installing one of the freely available time based authenticator apps. For example Google Authenticator or Authy.

An authenticator extension for customers is currently under development and will be released within a few months.
##Requirements
Tested on the latest Open Cart v.3.0.2.0. For other Open Cart versions, send us an e-mail or comment.
##Installation
- Download the zip file from the Open Cart Marketplace.
- Go to 'Extension' > 'Installer'.
- Use 'Upload File' to upload and automatically install the extension.
- Refresh the modification cache. Go to 'Extension' > 'Modifications'. Notice that Two Factor Authentication is now listed here and click the 'Refresh' button in the top right.
## Usage
After installation each administrator user can enable two factor authentication. Click on the profile button in the top right and click on Two Factor Authentication under Your Profile.
## Support
For questions, support and ideas, feel free to contact us at info@gelderblominternet.nl (Dutch and English).
## Tags
security, two factor, two factor authentication, multi factor authentication, authentication, google authenticator, authenticator, authenticator app