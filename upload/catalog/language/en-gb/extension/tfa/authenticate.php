<?php
// Heading
$_['heading_title']                = 'Two Factor Authentication';

// Text
$_['text_account']                 = 'Account';
$_['text_authenticate']            = 'Authenticate';
$_['text_authenticate_description'] = 'Please enter your two factor authentication code. Lost access to your phone? Enter your back-up instead.';
$_['text_forgotten']               = 'Forgotten Password';

// Entry
$_['entry_code']                  = 'Authentication code';

// Error
$_['error_authenticate']                  = 'Warning: No match for Code.';

// Button
$_['button_authenticate']                  = 'Authenticate';